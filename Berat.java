package com.company;

import java.util.Scanner;

public class Berat {
    public void menu(){
        System.out.println("\n ---------------------");
        System.out.println("|  Menu Satuan Berat  |");
        System.out.println(" ---------------------");
        System.out.println("| 1. Kilogram (kg)   |");
        System.out.println("| 2. Hektogram (hg)  |");
        System.out.println("| 3. Dekagram (dag)  |");
        System.out.println("| 4. Gram (g)        |");
        System.out.println("| 5. Desigram (dg)   |");
        System.out.println("| 6. Centigram (cg)  |");
        System.out.println("| 7. Miligram (mg)   |");
        System.out.println("| 8. Ton             |");
        System.out.println("| 9. Kwintal         |");
        System.out.println("| 10. Ons            |");
        System.out.println(" --------------------");
    }
    public void convertBerat(){
        Scanner input = new Scanner(System.in);
        double kg, hg, dag, g, dg, cg, mg, ton, kw, ons;
        menu();
        System.out.print("Pilih satuan awal : ");
        int pilih = input.nextInt();
        switch (pilih) {
            case 1 -> {
                System.out.print("Masukkan berat : ");
                double berat = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int x = input.nextInt();
                if (x == 1) {
                    kg = berat;
                    System.out.println("Hasil konversi berat : " + kg + " kg");
                } else if (x == 2) {
                    hg = berat * 10;
                    System.out.println("Hasil konversi berat : " + hg + " hg");
                } else if (x == 3) {
                    dag = berat * 100;
                    System.out.println("Hasil konversi berat : " + dag + " dag");
                } else if (x == 4) {
                    g = berat * 1000;
                    System.out.println("Hasil konversi berat : " + g + " g");
                } else if (x == 5) {
                    dg = berat * 10000;
                    System.out.println("Hasil konversi berat : " + dg + " dg");
                } else if (x == 6) {
                    cg = berat * 100000;
                    System.out.println("Hasil konversi berat : " + cg + " cg");
                } else if (x == 7) {
                    mg = berat * 1000000;
                    System.out.println("Hasil konversi berat : " + mg + " mg");
                } else if (x == 8){
                    ton = berat / 1000;
                    System.out.println("Hasil konversi berat : " + ton + " ton");
                } else if (x == 9){
                    kw = berat / 100;
                    System.out.println("Hasil konversi berat : " + kw + " kw");
                } else if (x == 10){
                    ons = berat * 10;
                    System.out.println("Hasil konversi berat : " + ons + " ons");
                } else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 2, 10 -> {
                System.out.print("Masukkan berat : ");
                double berat = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int x = input.nextInt();
                if (x == 1) {
                    kg = berat / 10;
                    System.out.println("Hasil konversi berat : " + kg + " kg");
                } else if (x == 2) {
                    hg = berat;
                    System.out.println("Hasil konversi berat : " + hg + " hg");
                } else if (x == 3) {
                    dag = berat * 10;
                    System.out.println("Hasil konversi berat : " + dag + " dag");
                } else if (x == 4) {
                    g = berat * 100;
                    System.out.println("Hasil konversi berat : " + g + " g");
                } else if (x == 5) {
                    dg = berat * 1000;
                    System.out.println("Hasil konversi berat : " + dg + " dg");
                } else if (x == 6) {
                    cg = berat * 10000;
                    System.out.println("Hasil konversi berat : " + cg + " cg");
                } else if (x == 7) {
                    mg = berat * 100000;
                    System.out.println("Hasil konversi berat : " + mg + " mg");
                } else if (x == 8){
                    ton = berat / 10000;
                    System.out.println("Hasil konversi berat : " + ton + " ton");
                } else if (x == 9){
                    kw = berat / 1000;
                    System.out.println("Hasil konversi berat : " + kw + " kw");
                } else if (x == 10){
                    ons = berat;
                    System.out.println("Hasil konversi berat : " + ons + " ons");
                } else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 3 -> {
                System.out.print("Masukkan berat : ");
                double berat = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int x = input.nextInt();
                if (x == 1) {
                    kg = berat / 100;
                    System.out.println("Hasil konversi berat : " + kg + " kg");
                } else if (x == 2) {
                    hg = berat / 10;
                    System.out.println("Hasil konversi berat : " + hg + " hg");
                } else if (x == 3) {
                    dag = berat;
                    System.out.println("Hasil konversi berat : " + dag + " dag");
                } else if (x == 4) {
                    g = berat * 10;
                    System.out.println("Hasil konversi berat : " + g + " g");
                } else if (x == 5) {
                    dg = berat * 100;
                    System.out.println("Hasil konversi berat : " + dg + " dg");
                } else if (x == 6) {
                    cg = berat * 1000;
                    System.out.println("Hasil konversi berat : " + cg + " cg");
                } else if (x == 7) {
                    mg = berat * 10000;
                    System.out.println("Hasil konversi berat : " + mg + " mg");
                } else if (x == 8){
                    ton = berat / 100000;
                    System.out.println("Hasil konversi berat : " + ton + " ton");
                } else if (x == 9){
                    kw = berat / 10000;
                    System.out.println("Hasil konversi berat : " + kw + " kw");
                } else if (x == 10){
                    ons = berat / 10;
                    System.out.println("Hasil konversi berat : " + ons + " ons");
                } else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 4 -> {
                System.out.print("Masukkan berat : ");
                double berat = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int x = input.nextInt();
                if (x == 1) {
                    kg = berat / 1000;
                    System.out.println("Hasil konversi berat : " + kg + " kg");
                } else if (x == 2) {
                    hg = berat / 100;
                    System.out.println("Hasil konversi berat : " + hg + " hg");
                } else if (x == 3) {
                    dag = berat / 10;
                    System.out.println("Hasil konversi berat : " + dag + " dag");
                } else if (x == 4) {
                    g = berat;
                    System.out.println("Hasil konversi berat : " + g + " g");
                } else if (x == 5) {
                    dg = berat * 10;
                    System.out.println("Hasil konversi berat : " + dg + " dg");
                } else if (x == 6) {
                    cg = berat * 100;
                    System.out.println("Hasil konversi berat : " + cg + " cg");
                } else if (x == 7) {
                    mg = berat * 1000;
                    System.out.println("Hasil konversi berat : " + mg + " mg");
                } else if (x == 8){
                    ton = berat / 1000000;
                    System.out.println("Hasil konversi berat : " + ton + " ton");
                } else if (x == 9){
                    kw = berat / 100000;
                    System.out.println("Hasil konversi berat : " + kw + " kw");
                } else if (x == 10){
                    ons = berat / 100;
                    System.out.println("Hasil konversi berat : " + ons + " ons");
                } else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 5 -> {
                System.out.print("Masukkan berat : ");
                double berat = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int x = input.nextInt();
                if (x == 1) {
                    kg = berat / 10000;
                    System.out.println("Hasil konversi berat : " + kg + " kg");
                } else if (x == 2) {
                    hg = berat / 1000;
                    System.out.println("Hasil konversi berat : " + hg + " hg");
                } else if (x == 3) {
                    dag = berat / 100;
                    System.out.println("Hasil konversi berat : " + dag + " dag");
                } else if (x == 4) {
                    g = berat / 10;
                    System.out.println("Hasil konversi berat : " + g + " g");
                } else if (x == 5) {
                    dg = berat;
                    System.out.println("Hasil konversi berat : " + dg + " dg");
                } else if (x == 6) {
                    cg = berat * 10;
                    System.out.println("Hasil konversi berat : " + cg + " cg");
                } else if (x == 7) {
                    mg = berat * 100;
                    System.out.println("Hasil konversi berat : " + mg + " mg");
                } else if (x == 8){
                    ton = berat / 10000000;
                    System.out.println("Hasil konversi berat : " + ton + " ton");
                } else if (x == 9){
                    kw = berat / 1000000;
                    System.out.println("Hasil konversi berat : " + kw + " kw");
                } else if (x == 10){
                    ons = berat / 1000;
                    System.out.println("Hasil konversi berat : " + ons + " ons");
                } else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 6 -> {
                System.out.print("Masukkan berat : ");
                double berat = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int x = input.nextInt();
                if (x == 1) {
                    kg = berat / 100000;
                    System.out.println("Hasil konversi berat : " + kg + " kg");
                } else if (x == 2) {
                    hg = berat / 10000;
                    System.out.println("Hasil konversi berat : " + hg + " hg");
                } else if (x == 3) {
                    dag = berat / 1000;
                    System.out.println("Hasil konversi berat : " + dag + " dag");
                } else if (x == 4) {
                    g = berat / 100;
                    System.out.println("Hasil konversi berat : " + g + " g");
                } else if (x == 5) {
                    dg = berat / 10;
                    System.out.println("Hasil konversi berat : " + dg + " dg");
                } else if (x == 6) {
                    cg = berat;
                    System.out.println("Hasil konversi berat : " + cg + " cg");
                } else if (x == 7) {
                    mg = berat * 10;
                    System.out.println("Hasil konversi berat : " + mg + " mg");
                } else if (x == 8){
                    ton = berat / 100000000;
                    System.out.println("Hasil konversi berat : " + ton + " ton");
                } else if (x == 9){
                    kw = berat / 10000000;
                    System.out.println("Hasil konversi berat : " + kw + " kw");
                } else if (x == 10){
                    ons = berat / 10000;
                    System.out.println("Hasil konversi berat : " + ons + " ons");
                } else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 7 -> {
                System.out.print("Masukkan berat : ");
                double berat = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int x = input.nextInt();
                if (x == 1) {
                    kg = berat / 1000000;
                    System.out.println("Hasil konversi berat : " + kg + " kg");
                } else if (x == 2) {
                    hg = berat / 100000;
                    System.out.println("Hasil konversi berat : " + hg + " hg");
                } else if (x == 3) {
                    dag = berat / 10000;
                    System.out.println("Hasil konversi berat : " + dag + " dag");
                } else if (x == 4) {
                    g = berat / 1000;
                    System.out.println("Hasil konversi berat : " + g + " g");
                } else if (x == 5) {
                    dg = berat / 100;
                    System.out.println("Hasil konversi berat : " + dg + " dg");
                } else if (x == 6) {
                    cg = berat / 10;
                    System.out.println("Hasil konversi berat : " + cg + " cg");
                } else if (x == 7) {
                    mg = berat;
                    System.out.println("Hasil konversi berat : " + mg + " mg");
                } else if (x == 8){
                    ton = berat / 100000000;
                    System.out.println("Hasil konversi berat : " + ton + " ton");
                } else if (x == 9){
                    kw = berat / 10000000;
                    System.out.println("Hasil konversi berat : " + kw + " kw");
                } else if (x == 10){
                    ons = berat / 10000;
                    System.out.println("Hasil konversi berat : " + ons + " ons");
                } else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 8 -> {
                System.out.print("Masukkan berat : ");
                double berat = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int x = input.nextInt();
                if (x == 1) {
                    kg = berat * 1000;
                    System.out.println("Hasil konversi berat : " + kg + " kg");
                } else if (x == 2) {
                    hg = berat * 10000;
                    System.out.println("Hasil konversi berat : " + hg + " hg");
                } else if (x == 3) {
                    dag = berat * 100000;
                    System.out.println("Hasil konversi berat : " + dag + " dag");
                } else if (x == 4) {
                    g = berat * 1000000;
                    System.out.println("Hasil konversi berat : " + g + " g");
                } else if (x == 5) {
                    dg = berat * 10000000;
                    System.out.println("Hasil konversi berat : " + dg + " dg");
                } else if (x == 6) {
                    cg = berat * 100000000;
                    System.out.println("Hasil konversi berat : " + cg + " cg");
                } else if (x == 7) {
                    mg = berat * 1000000000;
                    System.out.println("Hasil konversi berat : " + mg + " mg");
                } else if (x == 8){
                    ton = berat;
                    System.out.println("Hasil konversi berat : " + ton + " ton");
                } else if (x == 9){
                    kw = berat * 10;
                    System.out.println("Hasil konversi berat : " + kw + " kw");
                } else if (x == 10){
                    ons = berat * 10000;
                    System.out.println("Hasil konversi berat : " + ons + " ons");
                } else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 9 -> {
                System.out.print("Masukkan berat : ");
                double berat = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int x = input.nextInt();
                if (x == 1) {
                    kg = berat * 100;
                    System.out.println("Hasil konversi berat : " + kg + " kg");
                } else if (x == 2) {
                    hg = berat * 1000;
                    System.out.println("Hasil konversi berat : " + hg + " hg");
                } else if (x == 3) {
                    dag = berat * 10000;
                    System.out.println("Hasil konversi berat : " + dag + " dag");
                } else if (x == 4) {
                    g = berat * 100000;
                    System.out.println("Hasil konversi berat : " + g + " g");
                } else if (x == 5) {
                    dg = berat * 1000000;
                    System.out.println("Hasil konversi berat : " + dg + " dg");
                } else if (x == 6) {
                    cg = berat * 10000000;
                    System.out.println("Hasil konversi berat : " + cg + " cg");
                } else if (x == 7) {
                    mg = berat * 100000000;
                    System.out.println("Hasil konversi berat : " + mg + " mg");
                } else if (x == 8){
                    ton = berat / 10;
                    System.out.println("Hasil konversi berat : " + ton + " ton");
                } else if (x == 9){
                    kw = berat;
                    System.out.println("Hasil konversi berat : " + kw + " kw");
                } else if (x == 10){
                    ons = berat * 1000;
                    System.out.println("Hasil konversi berat : " + ons + " ons");
                } else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            default -> System.out.println("Pilihan Anda tidak tersedia!");
        }
    }
}

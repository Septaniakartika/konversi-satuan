package com.company;

import java.util.Scanner;

public class Jarak {
    public void menu(){
        System.out.println("\n ---------------------");
        System.out.println("|  Menu Satuan Jarak  |");
        System.out.println(" ---------------------");
        System.out.println("| 1. Kilometer (km)   |");
        System.out.println("| 2. Hektometer (hm)  |");
        System.out.println("| 3. Dekameter (dam)  |");
        System.out.println("| 4. Meter (m)        |");
        System.out.println("| 5. Desimeter (dm)   |");
        System.out.println("| 6. Centimeter (cm)  |");
        System.out.println("| 7. Milimeter (mm)   |");
        System.out.println(" --------------------");
    }
    public void convertJarak(){
        Scanner input = new Scanner(System.in);
        double km, hm, dam, m, dm, cm, mm;
        menu();
        System.out.print("Pilih satuan awal : ");
        int pilih = input.nextInt();
        switch (pilih){
            case 1 -> {
                System.out.print("Masukkan jarak : ");
                double jarak = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int x = input.nextInt();
                if (x == 1){
                    km = jarak;
                    System.out.println("Hasil konversi jarak : "+km+" km");
                }else if (x == 2){
                    hm = jarak*10;
                    System.out.println("Hasil konversi jarak : "+hm+" hm");
                }else if (x == 3){
                    dam = jarak*100;
                    System.out.println("Hasil konversi jarak : "+dam+" dam");
                }else if (x == 4){
                    m = jarak*1000;
                    System.out.println("Hasil konversi jarak : "+m+" m");
                }else if (x == 5){
                    dm = jarak*10000;
                    System.out.println("Hasil konversi jarak : "+dm+" dm");
                }else if (x == 6){
                    cm = jarak*100000;
                    System.out.println("Hasil konversi jarak : "+cm+" cm");
                }else if (x == 7){
                    mm = jarak*1000000;
                    System.out.println("Hasil konversi jarak : "+mm+" mm");
                }else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 2 -> {
                System.out.print("Masukkan jarak : ");
                double jarak = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int y = input.nextInt();
                if (y == 1){
                    km = jarak/10;
                    System.out.println("Hasil konversi jarak : "+km+" km");
                }else if (y == 2){
                    hm = jarak;
                    System.out.println("Hasil konversi jarak : "+hm+" hm");
                }else if (y == 3){
                    dam = jarak*10;
                    System.out.println("Hasil konversi jarak : "+dam+" dam");
                }else if (y == 4){
                    m = jarak*100;
                    System.out.println("Hasil konversi jarak : "+m+" m");
                }else if (y == 5){
                    dm = jarak*1000;
                    System.out.println("Hasil konversi jarak : "+dm+" dm");
                }else if (y == 6){
                    cm = jarak*10000;
                    System.out.println("Hasil konversi jarak : "+cm+" cm");
                }else if (y == 7){
                    mm = jarak*100000;
                    System.out.println("Hasil konversi jarak : "+mm+" mm");
                }else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 3 -> {
                System.out.print("Masukkan jarak : ");
                double jarak = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int y = input.nextInt();
                if (y == 1){
                    km = jarak/100;
                    System.out.println("Hasil konversi jarak : "+km+" km");
                }else if (y == 2){
                    hm = jarak/10;
                    System.out.println("Hasil konversi jarak : "+hm+" hm");
                }else if (y == 3){
                    dam = jarak;
                    System.out.println("Hasil konversi jarak : "+dam+" dam");
                }else if (y == 4){
                    m = jarak*10;
                    System.out.println("Hasil konversi jarak : "+m+" m");
                }else if (y == 5){
                    dm = jarak*100;
                    System.out.println("Hasil konversi jarak : "+dm+" dm");
                }else if (y == 6){
                    cm = jarak*1000;
                    System.out.println("Hasil konversi jarak : "+cm+" cm");
                }else if (y == 7){
                    mm = jarak*10000;
                    System.out.println("Hasil konversi jarak : "+mm+" mm");
                }else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 4 -> {
                System.out.print("Masukkan jarak : ");
                double jarak = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int y = input.nextInt();
                if (y == 1){
                    km = jarak/1000;
                    System.out.println("Hasil konversi jarak : "+km+" km");
                }else if (y == 2){
                    hm = jarak/100;
                    System.out.println("Hasil konversi jarak : "+hm+" hm");
                }else if (y == 3){
                    dam = jarak/10;
                    System.out.println("Hasil konversi jarak : "+dam+" dam");
                }else if (y == 4){
                    m = jarak;
                    System.out.println("Hasil konversi jarak : "+m+" m");
                }else if (y == 5){
                    dm = jarak*10;
                    System.out.println("Hasil konversi jarak : "+dm+" dm");
                }else if (y == 6){
                    cm = jarak*100;
                    System.out.println("Hasil konversi jarak : "+cm+" cm");
                }else if (y == 7){
                    mm = jarak*1000;
                    System.out.println("Hasil konversi jarak : "+mm+" mm");
                }else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 5 -> {
                System.out.print("Masukkan jarak : ");
                double jarak = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int y = input.nextInt();
                if (y == 1){
                    km = jarak/10000;
                    System.out.println("Hasil konversi jarak : "+km+" km");
                }else if (y == 2){
                    hm = jarak/1000;
                    System.out.println("Hasil konversi jarak : "+hm+" hm");
                }else if (y == 3){
                    dam = jarak/100;
                    System.out.println("Hasil konversi jarak : "+dam+" dam");
                }else if (y == 4){
                    m = jarak/10;
                    System.out.println("Hasil konversi jarak : "+m+" m");
                }else if (y == 5){
                    dm = jarak;
                    System.out.println("Hasil konversi jarak : "+dm+" dm");
                }else if (y == 6){
                    cm = jarak*10;
                    System.out.println("Hasil konversi jarak : "+cm+" cm");
                }else if (y == 7){
                    mm = jarak*100;
                    System.out.println("Hasil konversi jarak : "+mm+" mm");
                }else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 6 -> {
                System.out.print("Masukkan jarak : ");
                double jarak = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int y = input.nextInt();
                if (y == 1){
                    km = jarak/100000;
                    System.out.println("Hasil konversi jarak : "+km+" km");
                }else if (y == 2){
                    hm = jarak/10000;
                    System.out.println("Hasil konversi jarak : "+hm+" hm");
                }else if (y == 3){
                    dam = jarak/1000;
                    System.out.println("Hasil konversi jarak : "+dam+" dam");
                }else if (y == 4){
                    m = jarak/100;
                    System.out.println("Hasil konversi jarak : "+m+" m");
                }else if (y == 5){
                    dm = jarak/10;
                    System.out.println("Hasil konversi jarak : "+dm+" dm");
                }else if (y == 6){
                    cm = jarak;
                    System.out.println("Hasil konversi jarak : "+cm+" cm");
                }else if (y == 7){
                    mm = jarak*10;
                    System.out.println("Hasil konversi jarak : "+mm+" mm");
                }else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            case 7 -> {
                System.out.print("Masukkan jarak : ");
                double jarak = input.nextDouble();
                menu();
                System.out.print("Pilih satuan akhir : ");
                int y = input.nextInt();
                if (y == 1){
                    km = jarak/1000000;
                    System.out.println("Hasil konversi jarak : "+km+" km");
                }else if (y == 2){
                    hm = jarak/100000;
                    System.out.println("Hasil konversi jarak : "+hm+" hm");
                }else if (y == 3){
                    dam = jarak/10000;
                    System.out.println("Hasil konversi jarak : "+dam+" dam");
                }else if (y == 4){
                    m = jarak/1000;
                    System.out.println("Hasil konversi jarak : "+m+" m");
                }else if (y == 5){
                    dm = jarak/100;
                    System.out.println("Hasil konversi jarak : "+dm+" dm");
                }else if (y == 6){
                    cm = jarak/10;
                    System.out.println("Hasil konversi jarak : "+cm+" cm");
                }else if (y == 7){
                    mm = jarak;
                    System.out.println("Hasil konversi jarak : "+mm+" mm");
                }else
                    System.out.println("Pilihan Anda tidak tersedia!");
            }
            default -> System.out.println("Pilihan Anda tidak tersedia!");
        }
    }
}

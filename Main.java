package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Scanner input = new Scanner(System.in);
        Jarak j = new Jarak();
        Berat b = new Berat();
        System.out.println(" -------------------");
        System.out.println("|  KONVERSI SATUAN  |");
        System.out.println(" -------------------");
        System.out.println("| 1. Satuan Jarak   |");
        System.out.println("| 2. Satuan Berat   |");
        System.out.println(" -------------------");
        System.out.print("Masukkan pilihan : ");
        int pilih = input.nextInt();
        if (pilih == 1)
            j.convertJarak();
        else if (pilih == 2)
            b.convertBerat();
        else
            System.out.println("Pilihan Anda tidak tersedia!");
    }
}

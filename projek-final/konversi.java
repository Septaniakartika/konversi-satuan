import javax.swing.*;
import java.text.DecimalFormat;

public class konversi {
    private JComboBox cbUkur;
    private JPanel convert;
    private JComboBox cbUkur1;
    private JTextField jtUkur;
    private JTextField jtUkur2;
    private JButton hapusButton;
    private JButton keluarButton;
    private JButton konversiButton;

    public konversi() {
        konversiButton.addActionListener(e -> {
            //Konversi dari String ke Double
            // Variable a digunakan untuk menyimpan data atau nilai yang ada pada cbUkur maupun cbUkur1
            double a = Double.parseDouble(jtUkur.getText());
            //Variabel b digunakan untuk menyimpan data atau nilai yang ada pada Variable a
            double b;
            //agar tidak ada angka di belakang koma pada type data double
            DecimalFormat df = new DecimalFormat("#.##########");

            //DARI KM
            if (cbUkur.getSelectedItem()=="KM" && cbUkur1.getSelectedItem()=="KM"){
                b = a;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="KM" && cbUkur1.getSelectedItem()=="HM"){
                b = a*10;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="KM" && cbUkur1.getSelectedItem()=="DAM"){
                b = a*100;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="KM" && cbUkur1.getSelectedItem()=="M"){
                b = a*1000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="KM" && cbUkur1.getSelectedItem()=="DM"){
                b = a*10000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="KM" && cbUkur1.getSelectedItem()=="CM"){
                b = a*100000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="KM" && cbUkur1.getSelectedItem()=="MM"){
                b = a*1000000;
                jtUkur2.setText(" "+df.format(b));
            }

            //DARI HM
            if (cbUkur.getSelectedItem()=="HM" && cbUkur1.getSelectedItem()=="KM"){
                b = a/ 10;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="HM" && cbUkur1.getSelectedItem()=="HM"){
                b = a*1;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="HM" && cbUkur1.getSelectedItem()=="DAM"){
                b = a*10;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="HM" && cbUkur1.getSelectedItem()=="M"){
                b = a*100;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="HM" && cbUkur1.getSelectedItem()=="DM"){
                b = a*1000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="HM" && cbUkur1.getSelectedItem()=="CM"){
                b = a*10000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="HM" && cbUkur1.getSelectedItem()=="MM"){
                b = a*100000;
                jtUkur2.setText(" "+df.format(b));
            }

            // DARI DAM
            if (cbUkur.getSelectedItem()=="DAM" && cbUkur1.getSelectedItem()=="KM"){
                b = a/ 100;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="DAM" && cbUkur1.getSelectedItem()=="HM"){
                b = a/10;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="DAM" && cbUkur1.getSelectedItem()=="DAM"){
                b = a*1;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="DAM" && cbUkur1.getSelectedItem()=="M"){
                b = a*10;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="DAM" && cbUkur1.getSelectedItem()=="DM"){
                b = a*100;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="DAM" && cbUkur1.getSelectedItem()=="CM"){
                b = a*1000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="DAM" && cbUkur1.getSelectedItem()=="MM"){
                b = a*10000;
                jtUkur2.setText(" "+df.format(b));
            }

            // DARI M
            if (cbUkur.getSelectedItem()=="M" && cbUkur1.getSelectedItem()=="KM"){
                b = a/ 1000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="M" && cbUkur1.getSelectedItem()=="HM"){
                b = a/100;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="M" && cbUkur1.getSelectedItem()=="DAM"){
                b = a/10;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="M" && cbUkur1.getSelectedItem()=="M"){
                b = a*1;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="M" && cbUkur1.getSelectedItem()=="DM"){
                b = a*10;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="M" && cbUkur1.getSelectedItem()=="CM"){
                b = a*100;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="M" && cbUkur1.getSelectedItem()=="MM"){
                b = a*1000;
                jtUkur2.setText(" "+df.format(b));
            }

            // DARI DM
            if (cbUkur.getSelectedItem()=="DM" && cbUkur1.getSelectedItem()=="KM"){
                b = a/ 10000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="DM" && cbUkur1.getSelectedItem()=="HM"){
                b = a/1000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="DM" && cbUkur1.getSelectedItem()=="DAM"){
                b = a/100;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="DM" && cbUkur1.getSelectedItem()=="M"){
                b = a/10;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="DM" && cbUkur1.getSelectedItem()=="DM"){
                b = a*1;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="DM" && cbUkur1.getSelectedItem()=="CM"){
                b = a*10;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="DM" && cbUkur1.getSelectedItem()=="MM"){
                b = a*100;
                jtUkur2.setText(" "+df.format(b));
            }

            // DARI CM
            if (cbUkur.getSelectedItem()=="CM" && cbUkur1.getSelectedItem()=="KM"){
                b = a/ 100000;

                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="CM" && cbUkur1.getSelectedItem()=="HM"){
                b = a/10000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="CM" && cbUkur1.getSelectedItem()=="DAM"){
                b = a/1000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="CM" && cbUkur1.getSelectedItem()=="M"){
                b = a/100;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="CM" && cbUkur1.getSelectedItem()=="DM"){
                b = a/10;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="CM" && cbUkur1.getSelectedItem()=="CM"){
                b = a*1;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="CM" && cbUkur1.getSelectedItem()=="MM"){
                b = a*10;
                jtUkur2.setText(" "+df.format(b));
            }

            // DARI MM
            if (cbUkur.getSelectedItem()=="MM" && cbUkur1.getSelectedItem()=="KM"){
                b = a/ 1000000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="MM" && cbUkur1.getSelectedItem()=="HM"){
                b = a/100000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="MM" && cbUkur1.getSelectedItem()=="DAM"){
                b = a/10000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="MM" && cbUkur1.getSelectedItem()=="M"){
                b = a/1000;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="MM" && cbUkur1.getSelectedItem()=="DM"){
                b = a/100;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="MM" && cbUkur1.getSelectedItem()=="CM"){
                b = a/10;
                jtUkur2.setText(" "+df.format(b));
            }
            else if (cbUkur.getSelectedItem()=="MM" && cbUkur1.getSelectedItem()=="MM"){
                b = a*1;
                jtUkur2.setText(" "+df.format(b));
            }
        });

        hapusButton.addActionListener(e -> {
            jtUkur.setText("");
            jtUkur2.setText("");
            jtUkur2.requestFocus();
        });

        keluarButton.addActionListener(e -> System.exit(0));
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("konversi");
        frame.setContentPane(new konversi().convert);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
